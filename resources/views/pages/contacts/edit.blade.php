@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Contacts') }}</div>

                <div class="card-body">

                    @include('partials._msg')

                    <p>Edit contact</p>
                    @include('partials._form', ['action' => route('contacts.update', ['contact' => $contact])])

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
