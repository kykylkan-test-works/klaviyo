@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif
@if($msg = session()->pull('error'))
    <div class="alert alert-danger" role="alert">{{ $msg }}</div>
@endif
@if($msg = session()->pull('success'))
    <div class="alert alert-success" role="alert">{{ $msg }}</div>
@endif
@if($msg = session()->pull('queue'))
    <div class="alert alert-info" role="alert">{{ $msg }}</div>
@endif
