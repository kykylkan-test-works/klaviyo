<input class="form-control @error($inputName) is-invalid @enderror"
       placeholder="enter {{ $inputName }}"
       name="{{ $inputName }}"
       type="{{ $type ?? 'text' }}"
       value="{{ $contact->{$inputName} ?? old($inputName) }}"
>
@error($inputName)
<span class="invalid-feedback" role="alert">
    <strong>{{ $message }}</strong>
</span>
@enderror
