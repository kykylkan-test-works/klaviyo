@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if(auth()->check())
                        {{ __('You are logged in!') }}

                        <form method="post" action="{{ route('events.store') }}">
                            @csrf
                            <button type="submit" class="btn btn-primary">tracks clicked event</button>
                        </form>
                    @else
                        {{ __('You are not logged in!') }}
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
