<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    protected $fillable = [
        'phone',
        'email',
        'first_name',
        'last_name'
    ];

    protected $perPage = 5;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getIsSyncAttribute()
    {
        return !empty($this->person_id) ? 'yes' : 'no';
    }
}
