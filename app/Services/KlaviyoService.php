<?php

namespace App\Services;

use App\Models\Contact;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Klaviyo\Klaviyo as Klaviyo;
use Klaviyo\Model\EventModel as KlaviyoEvent;
use Klaviyo\Model\ProfileModel as KlaviyoProfile;

class KlaviyoService
{
	private $client;

    /**
     * KlaviyoService constructor.
     */
    public function __construct()
	{
        $this->client = new Klaviyo(
            config('services.klaviyo.private_key'),
            config('services.klaviyo.public_key')
        );
	}

    /**
     * @return mixed
     */
    public function getContacts()
    {
        // TODO: extra task - we can create a one List for each auth user and then add contacts to it

        return auth()->user()->contacts()->orderByDesc('id')->paginate();
    }

    /**
     * @param $contact
     * @return bool
     */
    public function contactsDelete($contact)
    {
        $result = false;

        try {
            if($contact->person_id) {
                $result = $this->client->dataprivacy->requestProfileDeletion($contact->person_id, 'person_id');

                if ($result) {
                    $contact->delete();
                }
            }
            else {
                $contact->delete();

                $result = true;
            }

            session()->flash('success', 'success contactsDelete.');
        } catch (\Exception $e) {
            Log::error($e->getMessage().' in '.$e->getFile().':'.$e->getLine());
            session()->flash('error', $e->getMessage());
        }

        return !empty($result);
    }

    /**
     * @param $data
     * @param $contact
     * @return bool
     */
    public function contactsUpdate($data, $contact)
    {
        // TODO: extra task - need a solution if update was failed by api

        $result = false;

        try {
            $contact->fill($data)->save();

            $params = [
                '$email' => $contact->email,
                '$phone_number' => $contact->phone,
                '$first_name' => $contact->first_name,
                '$last_name' => $contact->last_name,
            ];

            $result = $this->client->profiles->updateProfile($contact->person_id, $params);

            if (empty($contact->person_id) && isset($result['id']) && $result['object'] == 'person') {
                $contact->person_id = $result['id'];
                $contact->save();
            }

            session()->flash('success', 'success contactsUpdate.');
        } catch (\Exception $e) {
            Log::error($e->getMessage().' in '.$e->getFile().':'.$e->getLine());
            session()->flash('error', $e->getMessage());
        }

        return !empty($result);
    }

    /**
     * @param $data
     * @param null $user
     * @return bool
     */
    public function contactsCreate($data, $user = null)
    {
        $result = false;

        try {
            $user = $user ?? auth()->user();
            $contact = $user->contacts()->save(new Contact($data));

            $params = [
                '$email' => $contact->email,
                '$phone_number' => $contact->phone,
                '$first_name' => $contact->first_name,
                '$last_name' => $contact->last_name,
            ];

            $profile = new KlaviyoProfile($params);
            $result = $this->client->publicAPI->identify($profile);

            if (!empty($result) && empty($contact->person_id)) {
                $profile = $this->client->profiles->getProfileIdByEmail($contact->email);

                if (isset($profile['id'])) {
                    $contact->person_id = $profile['id'];
                    $contact->save();
                }
            }

            session()->flash('success', 'success contactsCreate.');
        } catch (\Exception $e) {
            Log::error($e->getMessage().' in '.$e->getFile().':'.$e->getLine());
            session()->flash('error', $e->getMessage());
        }

        return !empty($result);
    }

    /**
     * @param $user
     * @param $data
     * @return bool
     */
    public function contactsImport($data, $contact, $user)
    {
        $result = false;

        try {
            if ($contact && !empty($contact->person_id)) {
                $result = $this->contactsUpdate($data, $contact);
            }
            elseif(empty($contact)) {
                $result = $this->contactsCreate($data, $user);
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage().' in '.$e->getFile().':'.$e->getLine());
        }

        return !empty($result);
    }

    /**
     * @return bool
     */
    public function trackEvent()
	{
        $result = false;
        $dt = Carbon::now();
        $user = auth()->user();

        try {
            $params = [
                'event' => 'Clicked on button',
                'customer_properties' => [
                    '$email' => $user->email
                ],
                'properties' => [
                    "item_name" => "Button",
                    "clicked_time" => $dt->toDateTimeLocalString(),
                    '$value' => 100,
                ],
                'time' => $dt->timestamp
            ];

            $event = new KlaviyoEvent($params);
            $result = $this->client->publicAPI->track($event);

            if(!empty($result)) {
                session()->flash('success', 'success tracked the event.');
            }
            else {
                session()->flash('error', 'tracks the event.');
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage().' in '.$e->getFile().':'.$e->getLine());
            session()->flash('error', $e->getMessage());
        }

		return !empty($result);
	}
}
