<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Services\KlaviyoService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class TracksController extends Controller
{
    protected $klaviyoService;

    private const REDIRECT = 'contacts.index';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(KlaviyoService $klaviyoService)
    {
        $this->klaviyoService = $klaviyoService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $this->klaviyoService->trackEvent();

        return redirect()->route(self::REDIRECT);
    }
}
