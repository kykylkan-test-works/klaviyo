<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use App\Http\Requests\FileRequest;
use App\Jobs\ImportJob;
use App\Models\Contact;
use App\Services\KlaviyoService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\View\View;

class ContactsController extends Controller
{
    private KlaviyoService $klaviyoService;

    private const REDIRECT = 'contacts.index';

    /**
     * ContactsController constructor.
     * @param KlaviyoService $klaviyoService
     */
    public function __construct(KlaviyoService $klaviyoService)
    {
        $this->klaviyoService = $klaviyoService;
    }

    /**
     * @param FileRequest $request
     * @return RedirectResponse
     */
    public function import(FileRequest $request): RedirectResponse
    {
        if ($request->hasFile('import_file') && $request->file('import_file')->isValid()) {
            $fileName = Str::random(32).'.'.$request->import_file->extension();
            $path = $request->import_file->storeAs('', $fileName, 'import');

            ImportJob::dispatch(auth()->user(), $path);
            session()->flash('queue', 'Import was added to queue.');
        }

        return redirect()->route(self::REDIRECT);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): View
    {
        $data = $this->klaviyoService->getContacts();

        return view('pages.contacts.index', [
            'contacts' => $data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ContactRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ContactRequest $request): RedirectResponse
    {
        $this->klaviyoService->contactsCreate($request->only([
            'last_name',
            'first_name',
            'phone',
            'email'
        ]));

        return redirect()->route(self::REDIRECT);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ContactRequest $request, Contact $contact): RedirectResponse
    {
        $this->klaviyoService->contactsUpdate($request->only([
            'last_name',
            'first_name',
            'phone',
            'email'
        ]), $contact);

        return redirect()->route(self::REDIRECT);
    }

    /**
     * @param Contact $contact
     * @return View
     */
    public function edit(Contact $contact): View
    {
        return view('pages.contacts.edit', [
            'contact' => $contact
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Contact $contact): RedirectResponse
    {
        $this->klaviyoService->contactsDelete($contact);

        return redirect()->route(self::REDIRECT);
    }
}
