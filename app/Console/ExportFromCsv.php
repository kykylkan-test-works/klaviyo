<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Clickhouse;
use Illuminate\Support\Str;
use Illuminate\Console\Command;
use PhpClickHouseLaravel\RawColumn;
use Illuminate\Support\Facades\Storage;

class ExportFromCsv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:csv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $lockFile = 'csvImport.lock';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->lockFile = storage_path('export/'.$this->lockFile);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(file_exists($this->lockFile)) {
            echo __CLASS__ . ' is running!';
            exit;
        }

        if(!is_writable(storage_path('export'))) {
            echo storage_path('export') . ' must be writable!';
            exit;
        }

        file_put_contents($this->lockFile, Carbon::now()->toAtomString());

        try {
            $files = Storage::disk('export')->allFiles();

            echo "\nFound files: ".count($files)."\n";

            foreach ($files as $fileName) {
                if (Str::endsWith($fileName, '.csv')) {
                    echo "Processing file {$fileName}...\n";

                    $file = fopen(storage_path('export/' . $fileName), 'r');
                    $isCorrectData = false;
                    $i = 0;

                    while (($line = fgetcsv($file)) !== false) {
                        if ($isCorrectData) {

                            $rows = Clickhouse::select(['click_id'])->where('click_id', trim($line[0]))->getRows();

                            if(!empty($rows)) {
                                continue;
                            }

                            Clickhouse::create([
                                'id' => Str::uuid(),
                                'click_id' => trim($line[0]),
                                'conversions' => trim($line[9]),
                                'total_revenue' => trim($line[10]),
                                'is_ready' => 0
                            ]);

                            echo '.';

                            $i++;
                        }

                        if (!$isCorrectData && in_array('Total revenue', $line)) {
                            $isCorrectData = true;
                        }
                    }

                    fclose($file);

                    Storage::disk('export')->delete($fileName);

                    echo "\nFinished and deleted {$fileName}, saved: {$i} items.\n";
                }
            }
        } catch (\Exception $e) {
            dump($e->getMessage());
        }

        unlink($this->lockFile);
    }
}
