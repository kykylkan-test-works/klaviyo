<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('home');
});

Route::middleware(['auth'])->group(function () {
    Route::post('/events/store', [App\Http\Controllers\TracksController::class, 'store'])
        ->name('events.store');

    Route::prefix('contacts')->group(function () {
        Route::get('/', [App\Http\Controllers\ContactsController::class, 'index'])
            ->name('contacts.index');
        Route::get('/{contact}/edit', [App\Http\Controllers\ContactsController::class, 'edit'])
            ->name('contacts.edit');
        Route::post('/update/{contact}', [App\Http\Controllers\ContactsController::class, 'update'])
            ->name('contacts.update');
        Route::delete('/destroy/{contact}', [App\Http\Controllers\ContactsController::class, 'destroy'])
            ->name('contacts.destroy');
        Route::post('/store', [App\Http\Controllers\ContactsController::class, 'store'])
            ->name('contacts.store');
        Route::post('/import', [App\Http\Controllers\ContactsController::class, 'import'])
            ->name('contacts.import');
    });
});
